"use strict";
const express = require("express"),
  sysInfo = require("./utils/sys-info"),
  ParseServer = require("parse-server").ParseServer,
  fs = require("fs"),
  app = express();

process.env.NODE_ENV = process.env.NODE_ENV && process.env.NODE_ENV.trim();
app.set(
  "serverURL",
  process.env.NODE_ENV == "dev"
    ? "http://localhost:6000/_/"
    : "https://sliido.herokuapp.com/_/"
);

app.use(require("morgan")("dev"));
app.use(express.static(require("path").resolve(__dirname, "public")));

app.set("views", "views");
app.set("view engine", "ejs");
var parseApi = new ParseServer({
  databaseURI: process.env.MONGO_URL || "mongodb://localhost/sliido",
  cloud: require("path").resolve(__dirname, "./main.js"),

  //begin keys/////////////////////////////////////////////////
  appId: "2EW6UsN7MjCgrQ4XVv9w4Y6huUuXwrU6LFNnTMoXc",
  clientKey: "8npXZ1gOvqZJmT7dkLA2PudMhOgcZsJvgBRASw9k",
  javascriptKey: "UFhP5Fg7ZPG50do50898Cyv29nAyC6EIVyrAcmJehqs",
  restAPIKey: "0xTDfQTCgXRuyStU7ZEr4qHCcFMundefinediC5xhRtqYjoBe0I",
  dotNetKey: "dchh93LBgZiep3SfAcLN5PPTgAL46LVrkQJl8MmYbEQ",
  masterKey: "miXBE0TZ33usundefined5p4ilsraYb1iwqbWcBRdokDSRWundefinedjQ",
  //end keys//////////////////////////////////////////////

  serverURL: app.get("serverURL"),
  verifyUserEmails: true,
  emailVerifyTokenValidityDuration: 2 * 60 * 60,
  publicServerURL: app.get("serverURL"),
  appName: "Sliido",
  // The email adapter
  emailAdapter: {
    module: "parse-server-simple-mailgun-adapter",
    options: {
      // The address that your emails come from
      fromAddress: "no-reply@sliido.com",
      // Your domain from mailgun.com
      domain: "sliido.com",
      apiKey: "key-10e84d5300fd6c664f05df9bfe25d843"
    }
  },
  push: {
    android: {
      senderId: "554068366623",
      apiKey: "AIzaSyD_3YWXt_Q9VYaazcxJ2cL3kSdAtJcEKw4"
    }
  }
});

// IMPORTANT: Your application HAS to respond to GET /health with status 200
//            for OpenShift health monitoring
app.get("/health", (req, res) => {
  res.status(200).end();
});

app.get("/", (req, res, next) => {
  res.render("index");
});
app.get("/legal/terms", (req, res, next) => {
  res.render("terms");
});
app.get("/terms", (req, res, next) => {
  res.render("terms");
});
app.get("/legal/privacy", (req, res, next) => {
  res.render("privacy-policy");
});
app.get("/privacy", (req, res, next) => {
  res.render("privacy-policy");
});
app.get("/legal/opensource", (req, res, next) => {
  res.end("Not Found");
});

app.get("/dl", (req, res) => {
  res.redirect("https://play.google.com/store/apps/details?id=com.sliido");
});

//deep link redirections
app.get("/invites/ch", (req, res) => {
  res.redirect("sliido://invites/ch?id=" + req.query.id);
});
app.get("/ch", (req, res) => {
  res.redirect("sliido://invites/ch?id=" + req.query.id);
});

//these two endpoints override the parse api so
//must always be mounted before parse-server
app.get("/_/apps/verify_email_success.html", (req, res) => {
  res.redirect("sliido://verify_success");
});
app.get("/_/apps/password_reset_success.html", (req, res) => {
  res.redirect("sliido://password_reset");
});

//end deep link

app.use("/_", parseApi);

app.use(function(req, res) {
  res.status(404).end("NOT FOUND");
});

app.listen(process.env.PORT || 6000, function() {
  console.log(`Application worker ${process.pid}\
  listening on https:${process.env.PORT || 6000}...`);
});
