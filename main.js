"use strict";

var ONE_DAY = 1000 * 60 * 60 * 24;

function makeParseQuery(className) {
  var obj = Parse.Object.extend(className);
  return new Parse.Query(obj);
}

function getFollowers(post) {
  let likers = post.get("likers"),
    commenters = post.get("commenters");
  if (!likers && !commenters) return [];
  return likers ? likers.concat(commenters ? commenters : []) : commenters;
}

function PCallbackWrapper(cb) {
  return {
    useMasterKey: true,
    success: function(parseObjects) {
      //some parse queries like query#first() calls the success callback with undefined
      //when there is no match found
      if (!parseObjects)
        return cb({
          message: "Object Not Found",
          code: Parse.Error.OBJECT_NOT_FOUND
        });
      cb(null, parseObjects);
    },
    error: function(obj, error) {
      //some parse operations like save passes two params to the error callback
      //when only one params is passed error will be undefined so obj will denote the error
      error = error || obj;
      cb(error);
    }
  };
}

function newParseObject(className) {
  if (!className) throw new Error("className must be provided");
  var o = Parse.Object.extend(className);
  return new o();
}

Parse.Cloud.define("notifyLiked", (req, res) => {
  if (!req.user) {
    return res.error("Unauthorized");
  }
  if (!req.params.postId) {
    return res.error("post id required");
  }
  makeParseQuery("post")
    .get(req.params.postId)
    .then(post => {
      let followers = getFollowers(post);
      if (followers.length < 1) {
        console.log("no one following this post");
        return res.success();
      }
      var query = makeParseQuery("Installation");
      query.equalTo("enableNotification", 1); //respect notification settings
      query.containedIn("email", followers);
      sendPush(
        query,
        {
          type: "notification",
          body: {
            postId: post.id,
            by: req.user.id,
            action: "like",
            date: Date.now()
          }
        },
        ONE_DAY * 14
      );
      res.success();
    })
    .catch(err => {
      res.error(err);
    });
});

Parse.Cloud.define("createChannel", function(request, response) {
  var user = request.user;
  var user = request.user;

  if (!user)
    return response.error({
      code: Parse.Error.OPERATION_FORBIDDEN,
      message: "you are not authenticated"
    });

  var params = request.params;
  if (!params.name || params.isPublic === undefined)
    return response.error({
      code: Parse.Error.INVALID_JSON,
      message: "some required fields were not filled properly",
      data: JSON.stringify(request.params)
    });

  var query = makeParseQuery("channel");
  query.equalTo("nameUpper", params.name.toUpperCase());
  query.first(
    PCallbackWrapper(function(err, channel) {
      if (err) {
        if (err.code === Parse.Error.OBJECT_NOT_FOUND) {
          channel = newParseObject("channel");
          channel.set("isPublic", !!params.isPublic);
          if (params.coverPhoto) {
            channel.set("coverPhoto", params.coverPhoto);
          }
          channel.set("createdBy", user.getEmail());
          channel.set("nameUpper", params.name.toUpperCase());
          channel.set("name", params.name);
          channel.set("createdByName", user.get("name"));
          channel.set("subscribers", 0);
          channel.save(
            PCallbackWrapper(function(err, channel) {
              if (err) return response.error(err);
              return response.success({
                name: channel.get("name"),
                isSuper: "false",
                channelId: channel.id
              });
            })
          );
        } else {
          return response.error(err);
        }
      } else {
        return response.error({
          code: Parse.Error.DUPLICATE_VALUE,
          message: "This name is already taken"
        });
      }
    })
  );
});

Parse.Cloud.define("createPost", function(request, response) {
  var user = request.user;
  if (!user)
    return response.error({
      code: Parse.Error.OPERATION_FORBIDDEN,
      message: "not authenticated"
    });
  if (
    !request.params.postBody ||
    !request.params.channelId ||
    !request.params.channelName
  ) {
    console.log("submitted data was %s", JSON.stringify(request.params));
    return response.error({
      code: Parse.Error.INVALID_JSON,
      message: "some required fields were not found"
    });
  }

  var post = newParseObject("post");
  post.set("postBody", request.params.postBody);
  post.set("channelId", request.params.channelId);
  post.set("channelName", request.params.channelName);
  post.set("postedByName", user.get("name"));
  post.set("postedBy", user.getEmail());
  post.set("likes", 0);
  post.set("commentCount", 0);
  post.set("attachmentTitle", request.params.attachmentTitle);
  post.set("views", 0);
  post.set("attachment", request.params.attachment || "");
  makeParseQuery("channel")
    .get(request.params.channelId)
    .then(channel => {
      //the double negation is intentional
      post.set("whiteListed", !!channel.get("isSuper"));
      return post.save();
    })
    .then(obj => {
      var query = makeParseQuery("Installation");
      if (!obj.get("whiteListed")) {
        //non super channels
        query.equalTo("sleedoChannels", obj.get("channelId"));
      }

      query.equalTo("enableNotification", 1); //respect notification settings

      sendPush(
        query,
        {
          type: "post",
          body: {
            postId: obj.id
          }
        },
        ONE_DAY * 3
      );

      response.success({
        updatedAt: obj.updatedAt.getTime(),
        createdAt: obj.createdAt.getTime(),
        postId: obj.id,
        whiteListed: obj.get("whiteListed")
      });
    })
    .catch(err => {
      response.error(err);
    });
});

Parse.Cloud.afterSave("comment", function(request) {
  new Parse.Query("post")
    .get(request.object.get("postID"))
    .then(post => {
      post.increment("commentCount");
      post.addUnique("commenters", request.user.getEmail());
      return post.save();
    })
    .then(post => {
      let followers = getFollowers(post);
      if (followers.length < 1) {
        return console.log("no one following this post");
      }
      //send push to all those following this post
      var query = makeParseQuery("Installation");
      query.equalTo("enableNotification", 1); //respect notification settings
      query.containedIn("email", followers);

      sendPush(
        query,
        {
          type: "notification",
          body: {
            postId: post.id,
            by: request.user.id,
            action: "comment",
            date: Date.now()
          }
        },
        ONE_DAY * 14
      );
    })
    .catch(err => {
      console.log("error while looking up post to increment comment");
      console.log(err);
    });
});

function sendPush(query, data, expiresInterval) {
  console.log("sending %s via push", JSON.stringify(data));
  Parse.Push.send(
    {
      where: query,
      expiration_interval: expiresInterval || ONE_DAY,
      data: data
    },
    {
      useMasterKey: true,
      success: function() {
        console.log(data);
        console.log("push sent successfully");
      },
      error: function(err) {
        console.log("error while sending push notifications");
        console.log(err);
      }
    }
  );
}
